#pragma once

#include "definitions.h"
#include "mesh.h"

/*

nonlinear_terms_vector.h
Purpose: Define a class for containing nonlinear and linear terms of vector type, that enter in the SVEA approximation model.

@author Nuno Azevedo Silva
@version 1.0

*/

class nonlinear_terms_vector
{

public:

	std::vector<std::string> typenl;		//vector with string correspoding to the types of the nonlinearities - in this implementation type "Vector" only 
	std::vector<af::array> nonlinearities;	//vector with af::arrays for each nonlinearity
	std::vector<Precision> nonlinear_power;	//vector containing the powers of nonlinearities
	mesh& pmesh;							//mesh of our problem

	nonlinear_terms_vector(mesh& pmesh);

	nonlinear_terms_vector(mesh& pmesh, std::vector<std::string>  typenl, std::vector<af::array> nonlinearities, std::vector<Precision> nonlinear_power);

	void add_nonlinear_term_by_values(af::array nonlinearity, Precision nonlinear_power);

	template <typename Func>
	void add_nonlinear_term_by_function(Precision nonlinear_power, Func func);

	void load_nonlinearities(std::string loadDir, int nonlinear_pow, int step);

	void save_nonlinearities(std::string saveDir);

	inline bool isempty() {
		return this->nonlinearities.empty();
	}

};

/*
nonlinear_terms class constructor giving no terms

@param pmesh - mesh of our problem
@return Null
*/
nonlinear_terms_vector::nonlinear_terms_vector(mesh& pmesh) : pmesh(pmesh) {}

/*
nonlinear_terms class constructor giving the terms

@param pmesh - mesh of our problem
@param typenl - type of each nonlinearity
@param nonlinearities - nonlinear terms
@param nonlinear_power - nonlinear power for each term
@return Null
*/
nonlinear_terms_vector::nonlinear_terms_vector(mesh& pmesh, std::vector<std::string>  typenl, std::vector<af::array> nonlinearities, std::vector<Precision> nonlinear_power) :
	pmesh(pmesh), typenl(typenl), nonlinearities(nonlinearities), nonlinear_power(nonlinear_power) {}

//method to add a nonlinear term with values given by the user directly
void nonlinear_terms_vector::add_nonlinear_term_by_values(af::array nonlinearity, Precision nonlinear_pow)
{
	//add the term to the end of each array

	typenl.push_back("Vector");
	nonlinearities.push_back(nonlinearity);
	nonlinear_power.push_back(nonlinear_pow);

}

template <typename Func>
//method to add a nonlinear term using a function defined in functions_for_fields header
void nonlinear_terms_vector::add_nonlinear_term_by_function(Precision nonlinear_pow, Func func)
{
	//add the term to the end of each array

	typenl.push_back("Vector");
	nonlinear_power.push_back(nonlinear_pow);

	//compute the nonlinearity using the function
	af::array  nonlinearity = func;

	//and add it to the end of the nonlinearities vectors
	nonlinearities.push_back(nonlinearity);

}

//method for saving the nonlinearities
void nonlinear_terms_vector::save_nonlinearities(std::string saveDir)
{

	if (!isempty())
	{
		//if not created yet, create a new folder for the nonlinearities inside saveDir folder
		std::string newfolder = "\\Nonlinearities";
		std::string saveDir_new = saveDir + newfolder;
		CreateFolder(saveDir_new.c_str());

		//cycle for saving each nonlinearity
		for (int i = 0; i<typenl.size(); i++)
		{


			std::ostringstream convert;   // stream used for the conversion
			convert << i;      // insert the textual representation of 'Number' in the characters in the stream
			std::string Result = convert.str(); // set 'Result' to the contents of the stream

			std::string Path = saveDir_new + "\\NL_Vector_" + Result + ".af";

			// Save the array of the nonlinearity
			af::saveArray((std::string("NL")).c_str(), nonlinearities[i], Path.c_str(), false);

			// Save the nonlinearity properties
			std::string Path2 = saveDir_new + "\\NL_Vector_" + Result + ".txt";
			std::ofstream temp(Path2);
			temp << "TYPE \n" << typenl[i] << "\n";
			temp << "POWER \n" << nonlinear_power[i] << "\n";
			temp.close();

		}

	}
}


//method for loading a previous saved scalar field at step_number = step, in case anything goes wrong or for loading an externally generated solution
void nonlinear_terms_vector::load_nonlinearities(std::string loadDir,int nonlinear_pow, int step)
{

	//add the term to the end of each array


	std::string Path = loadDir + "NL_Vector_" + std::to_string(step) + ".af";

	// load a previous state
	af::array nonlinearity = af::readArray(Path.c_str(), "NL");
	typenl.push_back("Vector");

	nonlinearities.push_back(nonlinearity);
	nonlinear_power.push_back(nonlinear_pow);


}
