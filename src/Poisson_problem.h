#pragma once
/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/


 /*
 Poisson_problem.h
 Purpose: Contains, among other auxiliary functions, a push method
 that solves a given Poisson-like problem for a specic nonlocal_field object.
 */

#include "definitions.h"
#include "mesh.h"
#include "Misc.h"
#include "nonlocal_field.h"


class Poisson_problem
{

	/*
	This class implments a solver of the Poisson equation in the general form:

	C*Lap*O = D*O + E*|U|^2

	 using spectral methods.

	*/
public:

	nonlocal_field& field_nonlocal;	//the scalar field object
	mesh& pmesh;					//our mesh for the problem
	af::array& source;				//the nonlinearities object


	cPrecision c;					//parameter of the laplacian - see equation
	cPrecision d;					//parameter of the linear term - see equation
	cPrecision e;					//parameter of the source - see equation

	af::array k2;

	Poisson_problem(mesh& pmesh, nonlocal_field& field_nonlocal, af::array& source, cPrecision c, cPrecision d, cPrecision e);
	
	void Poisson_solver();

};

/*
Poisson_problem class constructor

@param pmesh is the mesh where we construct our scalar field vector
@param field_nonlocal is the profile, should be a previously created scalar field
@param l_factor is the number factor in the laplacian
@param nl_integrator is the type of integrator for the nonlinear step, "Euler"
@return Null
*/
Poisson_problem::Poisson_problem(mesh& pmesh, nonlocal_field& field_nonlocal, af::array& source, cPrecision c, cPrecision d, cPrecision e) :
	pmesh(pmesh), field_nonlocal(field_nonlocal), c(c), d(d), e(e), source(source)
{
	k2 = k2_nd(pmesh.dx, pmesh.dy, pmesh.dt, field_nonlocal.profile.dims(), Precision3);
	k2.eval();
}

void Poisson_problem::Poisson_solver()
{
	/*
	This function solves the poisson equation using spectral methods
	*/

	if (abs(d) != 0)
	{
		field_nonlocal.profile = e*af::dft(af::pow(af::abs(source), 2.0), 1.0, source.dims());
		field_nonlocal.profile *= -1 / (d + c*k2);
		field_nonlocal.profile *= 1.0 / (pmesh.Nx*pmesh.Ny*pmesh.Nt);
		field_nonlocal.profile = af::real(af::idft(field_nonlocal.profile, 1.0, field_nonlocal.profile.dims()));
	}
	else
	{
		//To prevent the division by 0.
		k2(0, 0, 0, 0) = 1;

		field_nonlocal.profile = e*af::dft(af::pow(af::abs(source), 2.0), 1.0, source.dims());
		field_nonlocal.profile *= -1 / (d + c*k2);
		field_nonlocal.profile *= 1.0 / (pmesh.Nx*pmesh.Ny*pmesh.Nt);
		field_nonlocal.profile(0, 0, 0, 0) = 0;
		field_nonlocal.profile = af::real(af::idft(field_nonlocal.profile, 1.0, field_nonlocal.profile.dims()));

	};

	field_nonlocal.eval();
};
