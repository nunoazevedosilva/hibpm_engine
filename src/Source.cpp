/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/

 /*
Source.cpp
 */


#include "core.h"
#include "sample_GNLSP_problem.h"

using namespace af;


int main2(int argc, char *argv[])
{

	// Select backend

	std::cout << "Select Backend" << std::endl;
	std::cout << "1.CPU" << std::endl;
	std::cout << "2.OPENCL" << std::endl;
	std::cout << "3.CUDA" << std::endl;
	int backend;
	std::cout << "Select Backend (number): ";
	std::cin >> backend;
	std::cout << std::endl;
	if (backend == 1) { af_set_backend(AF_BACKEND_CPU); }
	else if (backend == 2) { af_set_backend(AF_BACKEND_OPENCL); }
	else if (backend == 3) { af_set_backend(AF_BACKEND_CUDA); }
	else { std::cout << "Error - invalid backend" << std::endl; }	

	// Select a device after displaying info
	af::info();

	//select the device
	int device;
	std::cout << "Select device number: ";
	std::cin >> device;
	std::cout<<std::endl;
	af::setDevice(device);

	//select Backend


	std::cout << "Running info";
	af::info();

	//running an example
	example_scat_2d("data_folder",10);

	return 0;
}

int main(int argc, char *argv[])
{

	// Select backend
	
	//std::cout << "Select Backend" << std::endl;
	//std::cout << "1.CPU" << std::endl;
	//std::cout << "2.OPENCL" << std::endl;
	//std::cout << "3.CUDA" << std::endl;
	//int backend;
	//std::cout << "Select Backend (number): ";
	//std::cin >> backend;
	//std::cout << std::endl;
	//if (backend == 1) { af_set_backend(AF_BACKEND_CPU); }
	//else if (backend == 2) { af_set_backend(AF_BACKEND_OPENCL); }
	//else if (backend == 3) { af_set_backend(AF_BACKEND_CUDA); }
	//else { std::cout << "Error - invalid backend" << std::endl; }
	
	int backend = std::stoi(argv[1]);
	if (backend == 1) { af_set_backend(AF_BACKEND_CPU); }
	else if (backend == 2) { af_set_backend(AF_BACKEND_OPENCL); }
	else if (backend == 3) { af_set_backend(AF_BACKEND_CUDA); }
	else { std::cout << "Error - invalid backend" << std::endl; }

	
	// Select a device after displaying info
	//af::info();

	//select the device
	//int device;
	int device = std::stoi(argv[2]);
	//std::cout << "Select device number: ";
	//std::cin >> device;
	//std::cout << std::endl;
	//af::setDevice(device);
	//select Backend

	std::cout << "Running info";
	af::info();
	


	//Load configuration file

	std::string load_dir = "config_folder\\";

	std::string line;
	std::ifstream config_file(load_dir+"config_file.txt");
	
	
		getline(config_file, line);
		int dims = std::stoi(line);

		getline(config_file, line);
		int Nx = std::stoi(line);

		getline(config_file, line);
		int Ny = std::stoi(line);

		getline(config_file, line);
		int Nt = std::stoi(line);

		getline(config_file, line);
		int Nz = std::stoi(line);

		getline(config_file, line);
		int total_steps = std::stoi(line);

		getline(config_file, line);
		int stride = std::stoi(line);

		getline(config_file, line);
		double dx = std::stod(line);

		getline(config_file, line);
		double dy = std::stod(line);

		getline(config_file, line);
		double dz = std::stod(line);

		getline(config_file, line);
		double dt = std::stod(line);

		getline(config_file, line);
		int number_nl_numbers = std::stoi(line);

		getline(config_file, line);
		int number_nl_vectors = std::stoi(line);

		//save dir
		
		getline(config_file, line);
		std::string saveDir = line;

		CreateFolder(saveDir.c_str());


		config_file.close();
	



	//create a mesh
	mesh mymesh = mesh(dims, Nx, Ny, Nt, Nz, dx, dy, dt, dz);

	//add nonlinearities - load from initial conditions

	//numbers
	nonlinear_terms_number NL_number = nonlinear_terms_number(mymesh);


	std::ifstream nl_file(load_dir+"nl_numbers.txt");
	for (int nlv = 0; nlv < number_nl_numbers; nlv++)
	{
		getline(nl_file, line);
		Precision nl_real = std::stod(line);

		getline(nl_file, line);
		Precision nl_imag = std::stod(line);

		getline(nl_file, line);
		int power = std::stoi(line);

		NL_number.add_nonlinear_term_by_values(cPrecision(nl_real,nl_imag), Precision(power));

	}
	nl_file.close();

	//vectors - if existent

	std::ifstream nlv_file(load_dir + "nl_vectors.txt");
	nonlinear_terms_vector NL_vector = nonlinear_terms_vector(mymesh);
	for (int nlv = 0; nlv < number_nl_vectors; nlv++)
	{
			getline(nlv_file, line);
			int power = std::stoi(line);
			NL_vector.load_nonlinearities(load_dir, power, nlv);
	}
	config_file.close();

	//add field - load from initial conditions
	

	//setup the system 
		//initialize a scalar field

	gnlse_field envelope = gnlse_field(mymesh, 0);
	envelope.load_field(load_dir, 0);

	printf("---scalar field created--- \n");

	


	//getchar();

	printf("Creating a mesh for our problem \n");

	//define a new mesh for the problem


	printf("---Mesh created--- \n");


	//create the nonlinearities
	nonlinear_terms NL = nonlinear_terms(mymesh, NL_number, NL_vector);

	printf("---Nonlinearities created--- \n");


	//creating the GNLSE problem
	GNLSE_problem problem = GNLSE_problem(mymesh, envelope, NL, 0.5);

	//window creation
	const static int width = 512, height = 512;
	af::Window window3(width, height, "2D plot example title");


	af::sync();
	printf("---start--- \n");

	//clock initial
	auto start = std::chrono::high_resolution_clock::now();

	//integration routine
	problem.solve(stride, total_steps, saveDir, window3);

	//clock final
	auto end = std::chrono::high_resolution_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
	std::cout << "Total elapsed time " << "\t" << elapsed.count() << std::endl;


	return 0;

}