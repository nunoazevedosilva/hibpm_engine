#pragma once

/************************************************
 *  HiBPM: a numerical module to solve the Generalized
 Nonlinear Schrödinger Poisson equation system(GNLSP).
 * author: Nuno Azevedo Silva and Tiago David Ferreira
 * nuno.a.silva@inesctec.pt
 * tiagodsilvaf@gmail.com
 ***********************************************/


 /*
nonlocal_field.h
 Purpose: This class creates an instance to wrap the information of a
discretized array on top of the mesh corresponding to the nonlocal terms field;
 */


#include "definitions.h"
#include "mesh.h"

class nonlocal_field 
{

public:

	af::array profile;	//vector to store the profile 
	mesh& pmesh;		//mesh for the problem


	nonlocal_field(mesh& pmesh);

	void save_field(std::string saveDir);

	void load_field(std::string loadDir, int step);

	template <typename Func>
	void add_field(Func func);

	void plot(af::Window& window);

	/*
	Force af::array evaluation
	*/
	inline void eval() {
		this->profile.eval();
	}


	inline bool isempty() {
		return this->profile.isempty();
	}

};

/*
nonlocal_field class constructor

@param pmesh is the mesh where we construct our scalar field vector for the GNLSE problem
@return Null
*/
nonlocal_field::nonlocal_field(mesh& pmesh): pmesh(pmesh)
{
	//initialize the array to store the profile
	profile = af::constant(0., pmesh.Nx, pmesh.Ny, pmesh.Nt, 1, Precision2);

}

template <typename Func>
//method to add a profile using a function defined as in functions_for_fields header
void nonlocal_field::add_field(Func func)
{
	//compute the field to add using the function
	af::array  add_p = func;

	//and add it to the end of the profile vector
	profile += add_p;

	profile.eval();
	
}

//method for saving the scalar field
void nonlocal_field::save_field(std::string saveDir)
{
	std::string Path = saveDir + "\\SF_"+std::to_string(pmesh.current_step_number)+".af";

	/// Save current state of the arrays
	af::saveArray(std::string("SF").c_str(), profile, Path.c_str(), false);

}

//method for loading a previous saved scalar field at step_number = step, in case anything goes wrong or for loading an externally generated solution
void nonlocal_field::load_field(std::string loadDir, int step)
{
	std::string Path = loadDir + "\\SF_"+std::to_string(step)+".af";

	// load a previous state
	profile = af::readArray(Path.c_str(), "SF");
	pmesh.current_step_number = step;

}

//method for plotting during a simulation
void nonlocal_field::plot(af::Window& win)
{

	if (pmesh.dims == 1)//1d plots
	{

		af::array X = pmesh.x()(af::span, 0, 0, 0).as(f32);
		af::array Y = af::abs(profile(af::span, 0)).as(f32);//((af::abs(profile(af::span, 0)*1)-af::abs(0*profile(af::span, 0)+1))*1).as(f32);
		win.plot(X, Y);
	
	}
	
	if (pmesh.dims == 2)//2d plots
	{

		//findmax procedure
		Precision value;
		unsigned int index;
		af::max<Precision>(&value, &index, af::abs(profile));

		af::array Z =  af::transpose((af::abs(profile(af::span,af::span, 0, 0))).as(f32));
		//af::array Z = af::abs(profile(af::span, af::span, 0, 0)).as(f32);
		Z(af::span, 0, 0, 0) = 0;
		Z(af::span, -1, 0, 0) = 0;
		Z(0,af::span, 0, 0) = 0;
		Z(-1,af::span, 0, 0) = 0;
		win.setAxesLimits(0, pmesh.Lx, 0, pmesh.Ly);
		win.setColorMap(AF_COLORMAP_MAGMA);
		win.image(Z);
		//win.surface(pmesh.x()(af::span, af::span, 0,0).as(f32), pmesh.y()(af::span, af::span, 0,0).as(f32),Z);
		//win.setColorMap(AF_COLORMAP_BLUE);
		
	}

	if (pmesh.dims == 3)//3d plots - unavailable for now
	{
		//af::array X = pmesh.x()(af::span, 0, 0).as(f32);
		//af::array Y = pmesh.y()(0, af::span, 0).as(f32);

		//findmax procedure
		Precision value;
		unsigned int index;
		af::max<Precision>(&value, &index, af::abs(profile));

		af::array Z = af::transpose((af::abs(profile(af::span, af::span, 0, 0))).as(f32));
		//af::array Z = af::abs(profile(af::span, af::span, 0, 0)).as(f32);
		Z(af::span, 0, 0, 0) = 0;
		Z(af::span, -1, 0, 0) = 0;
		Z(0, af::span, 0, 0) = 0;
		Z(-1, af::span, 0, 0) = 0;
		win.setColorMap(AF_COLORMAP_INFERNO);
		win.image(Z);
		//win.surface(pmesh.x()(af::span, af::span, 0,0).as(f32), pmesh.y()(af::span, af::span, 0,0).as(f32),Z);
		//win.setColorMap(AF_COLORMAP_BLUE);

	}

}

