#pragma once

#include "definitions.h"
#include "mesh.h"

/**

nonlinear_terms_number.h
Purpose: Define a class for containing number nonlinear and linear terms, that enter in the SVEA approximation model.

@author Nuno Azevedo Silva
@version 1.0

*/

class nonlinear_terms_number
{

public:

	std::vector<std::string> typenl;		//vector with string correspoding to the types of the nonlinearities - in this implementation it will be allways of type "Number"
	std::vector<cPrecision> nonlinearities;	//vector with complex numbers for each nonlinearity
	std::vector<Precision> nonlinear_power;	//vector containing the powers of nonlinearities
	mesh& pmesh;							//mesh of our problem


	nonlinear_terms_number(mesh& pmesh);

	nonlinear_terms_number(mesh& pmesh, std::vector<std::string>  typenl, std::vector<cPrecision> nonlinearities, std::vector<Precision> nonlinear_power);

	void add_nonlinear_term_by_values(cPrecision nonlinearity, Precision nonlinear_power);

	void save_nonlinearities(std::string saveDir);

	inline bool isempty() {
		return this->nonlinearities.empty();
	}


};

/*
nonlinear_terms class constructor giving no terms

@param pmesh - mesh of our problem
@return Null
*/
nonlinear_terms_number::nonlinear_terms_number(mesh& pmesh) : pmesh(pmesh) {}

/*
nonlinear_terms class constructor giving the terms

@param pmesh - mesh of our problem
@param typenl - type of each nonlinearity
@param nonlinearities - nonlinear term value
@param nonlinear_power - nonlinear power for each term
@return Null
*/
nonlinear_terms_number::nonlinear_terms_number(mesh& pmesh, std::vector<std::string>  typenl, std::vector<cPrecision> nonlinearities, std::vector<Precision> nonlinear_power) :
	pmesh(pmesh), typenl(typenl), nonlinearities(nonlinearities), nonlinear_power(nonlinear_power) {}

//method to add a nonlinear term with values given by the user directly
void nonlinear_terms_number::add_nonlinear_term_by_values(cPrecision nonlinearity, Precision nonlinear_pow)
{
	//add the term to the end of each array
	typenl.push_back("Number");
	nonlinearities.push_back(nonlinearity);
	nonlinear_power.push_back(nonlinear_pow);

}

//method for saving the nonlinearities
void nonlinear_terms_number::save_nonlinearities(std::string saveDir)
{
	if (! this->isempty())
	{
		//if not created yet, create a new folder for the nonlinearities inside saveDir folder

		std::string newfolder = "\\Nonlinearities";
		std::string saveDir_new = saveDir + newfolder;
		CreateFolder(saveDir_new.c_str());

		//cycle for saving each nonlinearity
		for (int i = 0; i<typenl.size(); i++)
		{
			std::ostringstream convert;   // stream used for the conversion
			convert << i;      // insert the textual representation of 'Number' in the characters in the stream
			std::string Result = convert.str(); // set 'Result' to the contents of the stream

			// Save the nonlinearity properties
			std::string Path = saveDir_new + "\\NL_Number_" + Result + ".txt";
			std::ofstream temp(Path);
			temp << "TYPE \n" << typenl[i] << "\n";
			temp << "POWER \n" << nonlinear_power[i] << "\n";
			temp << "VALUE \n" << nonlinearities[i] << "\n";
			temp.close();
		}
	}
}

